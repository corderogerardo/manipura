<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('correo');
            $table->string('pass',60);
            $table->rememberToken();
            $table->timestamps();
            $table->string('estado');
            $table->foreign('correo')->references('correo')->on('personas');
            $table->integer('idrol')->unsigned();
            $table->foreign('idrol')->references('id')->on('rols');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('usuarios');
    }
}
