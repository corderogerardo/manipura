@extends('layouts.principal')
@section('content')
<div class="white-spacex"></div>
<div id="contenedor-default">

	<div id="proximo-evento">
	<!-- Inicio de columna izquierda -->
		<div id="left-column">
		<h2 class="event-title-e">
	<img src="imgs/alphabet-material/e.png" alt="">
	<img src="imgs/alphabet-material/n.png" alt="">
	<img src="imgs/alphabet-material/s.png" alt="">
	<img src="imgs/alphabet-material/e.png" alt="">
	<img src="imgs/alphabet-material/n.png" alt="">
	<img src="imgs/alphabet-material/a.png" alt="">
	<img src="imgs/alphabet-material/n.png" alt="">
	<img src="imgs/alphabet-material/z.png" alt="">
	<img src="imgs/alphabet-material/a.png" alt="">
	&nbsp;&nbsp;&nbsp;
	<img src="imgs/alphabet-material/e.png" alt="">
	<img src="imgs/alphabet-material/s.png" alt="">
	<img src="imgs/alphabet-material/p.png" alt="">
	<img src="imgs/alphabet-material/i.png" alt="">
	<img src="imgs/alphabet-material/r.png" alt="">
	<img src="imgs/alphabet-material/i.png" alt="">
	<img src="imgs/alphabet-material/t.png" alt="">
	<img src="imgs/alphabet-material/u.png" alt="">
	<img src="imgs/alphabet-material/a.png" alt="">
	<img src="imgs/alphabet-material/l.png" alt="">

	</h2>
	<h2 class="event-title">
	<img src="imgs/alphabet-material/m.png" alt="">
	<img src="imgs/alphabet-material/e.png" alt="">
	<img src="imgs/alphabet-material/t.png" alt="">
	<img src="imgs/alphabet-material/a.png" alt="">
	<img src="imgs/alphabet-material/f.png" alt="">
	<img src="imgs/alphabet-material/i.png" alt="">
	<img src="imgs/alphabet-material/s.png" alt="">
	<img src="imgs/alphabet-material/i.png" alt="">
	<img src="imgs/alphabet-material/c.png" alt="">
	<img src="imgs/alphabet-material/a.png" alt="">
	</h2>
		<h2 class="event-subtitle">Siete Cuerpos y Planos</h2>
		<h3 class="event-subtitle">Facilitadora: Magaly Prada. Grupo Metafísico Arcángel Miguel - LARA.</h3>
		<div class="event-body">
			Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
		</div>
		<div id="button-content">	
			<a class="button-jaffa-white" href="" title="">Reserva</a></div>
		<div class="social-networks-events">
			
			<a href="" title="">
			<img src="imgs/social/facebook.png" alt="">
			</a>
			Entrada Libre - Donación Amorosa.
			<a href="" title="">
			<img src="imgs/social/Twitter.png" alt="">
			</a>
		</div>
		<!-- fin de columna izquierda -->
		<!-- inicio de columna derecha  -->
		</div>
		<div id="right-column">
		<h2 class="event-title">
	<img src="imgs/alphabet-material/e.png" alt="">
	<img src="imgs/alphabet-material/v.png" alt="">
	<img src="imgs/alphabet-material/e.png" alt="">
	<img src="imgs/alphabet-material/n.png" alt="">
	<img src="imgs/alphabet-material/t.png" alt="">
	<img src="imgs/alphabet-material/o.png" alt="">
	<img src="imgs/alphabet-material/s.png" alt="">
	</h2>
			<h4 class="event-date">Hora: 10:00am - 20 / 08 / 2015</h4>
			<h5 class="event-place">Ubicacion: Carrera 20 con calle 10 centro Empresarial Leonardo Da Vinci piso 2. Entrada por la carrera 21. Barquisimeto Estado Lara, Venezuela.</h5>
			<h5 class="event-place">Telefonos: 0414-4210552 / 0414-3967124 / 0412-3024248</h5>
			<div id="img-maps">
				<iframe src="https://mapsengine.google.com/map/embed?mid=zpSiJEMQtliY.kt-lv8KD4mBY" width="98%" height="98%"></iframe>
			</div>

			
		</div>

		
		
	</div>
	
</div>
<!--Inicio de la seccion de Productos -->
<section id="contenedor-default">
	<h2 class="nos-services">SERVICIOS</h2>

	<div id="cont-serv">

		<div class="serv-left">
			<div class="serv-tp">
				<img src="imgs/serv/events.png" alt="">
				<h2 class="title-serv-one
">EVENTOS</h2>
			</div>

			<div class="serv-tp">
				<img src="imgs/serv/asesoria.png" alt="">
				<h2 class="title-serv-two
">ASESORIAS ESPIRITUALES</h2>
			</div>
			
			<div class="serv-tp">
				<img src="imgs/serv/espiritual.png" alt="">
				<h2 class="title-serv-three
">LIMPIEZAS ESPIRITUALES</h2>
			</div>


			
		</div>
		<div class="serv-right">
		<img src="imgs/serv/budha.jpg" alt="">
			<div class="serv-body">
				
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
				

			</div>
			<div class="cont-button">
					<a class="button-crusta">
					Mas informacion...
					</a>
				</div>

			
		</div>
	
	</div>
</section>
<!-- Fin de la seccion de Productos -->
<!-- Inicio de la seccion de servicios -->
<section id="contenedor-default">

	<h2 class="producto-title">PRODUCTOS</h2>
	
	<div id="cont-prod">
	<!-- Producto  -->
		<div id="product">
			<img src="imgs/product/libroespiritual.jpg" alt="">
			<p></p>
			<div id="title-prod">
			<a href="" title="">LIBROS DE MATAFISICA 
			<i class="fa fa-external-link-square"></i></a>
			</div>
		</div>
		<!-- producto -->
		<div id="product">
			<img src="imgs/product/exceptchange.jpg" alt="">
			<p></p>
			<div id="title-prod">
			<a href="" title="">POSTER DE MATAFISICA 
			<i class="fa fa-external-link-square"></i></a>
			</div>
		</div>
		<!-- producto -->
		<div id="product">
			<img src="imgs/product/velas-aromaticas.jpg" alt="">
			<p></p>
			<div id="title-prod">
			<a href="" title="">VELAS DE AROMATICAS 
			<i class="fa fa-external-link-square"></i></a>
			</div>
		</div>
		<!-- Producto -->
		<div id="product">
			<img src="imgs/product/libroespiritual.jpg" alt="">
			<p></p>
			<div id="title-prod">
			<a href="" title="">LIBROS DE MATAFISICA 
			<i class="fa fa-external-link-square"></i></a>
			</div>
		</div>
	
	<!-- Producto  -->
		<div id="product">
			<img src="imgs/product/libroespiritual.jpg" alt="">
			<p></p>
			<div id="title-prod">
			<a href="" title="">LIBROS DE MATAFISICA <i class="fa fa-arrow-circle-right"></i></a>
			</div>
		</div>
		<!-- producto -->
		<div id="product">
			<img src="imgs/product/libroespiritual.jpg" alt="">
			<p></p>
			<div id="title-prod">
			<a href="" title="">POSTER DE MATAFISICA <i class="fa fa-arrow-circle-right"></i></a>
			</div>
		</div>
		<!-- producto -->
		<div id="product">
			<img src="imgs/product/libroespiritual.jpg" alt="">
			<p></p>
			<div id="title-prod">
			<a href="" title="">LIBROS DE MATAFISICA <i class="fa fa-arrow-circle-right"></i></a>
			</div>
		</div>
		<!-- Producto -->
		<div id="product">
			<img src="imgs/product/libroespiritual.jpg" alt="">
			<p></p>
			<div id="title-prod">
			<a href="" title="">LIBROS DE MATAFISICA <i class="fa fa-arrow-circle-right"></i></a>
			</div>
		</div>
	</div>

</section>
<!-- Fin de servicios -->
<!-- Inicio de contacto -->
<section id="contenedor-default">
<h2 class="contac-title">CONTACTO</h2>

<div id="cont-contac">

	<div id="form-contac">
		<form action="index_submit" method="POST" accept-charset="utf-8">

		<div class="fil-one">
			<input type="text" name="nombre"  placeholder="Nombre" required>
			<input type="email" name="email"  placeholder="Email" required>
			<input type="tel" name="tel"  placeholder="Telefono" required>
			<input type="text" name="motivo" placeholder="Motivo" required>
		</div>
		<div class="fil-two">
			<textarea name="mensaje" placeholder="Mensaje"></textarea>
		
		</div>
		<div class="cont-button-send">
			<button class="button-send" type="submit">ENVIAR</button>
		</div>
		</form>
		
	</div>

	<div id="contac-img">
	<h3>Manipura</h3>
	<h5>Articulos de Metafisica , Libros , Posters , Información , Todo lo necesario para que el conocimiento llegue a ti...</h5>
	<h5>Cabudare - Lara - Venezuela.</h5>
		<div class="fil-info">
			<i class="fa fa-phone"></i>
			<a href="tel:+582512620402" target="_blank" title="">+58251-2620402</a>
		</div>
		<div class="fil-info">
			<i class="fa fa-mobile"></i>
			<a href="tel:+584122620402" target="_blank" title="">+58412-2620402</a>
		</div>
		<div class="fil-info">
			<i class="fa fa-twitter"></i>
			<a href="https://twitter.com/Manipuralara" target="_blank" title="@manipuralara">@Manipuralara</a>
		</div>
		<div class="fil-info">
			<i class="fa fa-facebook"></i>
			<a href="https://www.facebook.com/profile.php?id=100005412798055" target="_blank" title="Facebook Manipura">Facebook Manipura</a>
		</div>
		<div class="fil-info">
			<i class="fa fa-skype"></i>
			<a href="skype:manipura_lara?call" target="_blank" title="">manipura_lara</a>
		</div>
		<div class="fil-info">
			<i class="fa fa-envelope"></i>
		<a href="mailto:contacto@manipura.com.ve?subject=feedback" target="_blank" "email me">contacto@manipura.com.ve</a>
		</div>
		
	</div>
	
</div>

</section>
<!-- Fin de contacto -->
@stop