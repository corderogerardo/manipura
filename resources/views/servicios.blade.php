@extends('layouts.principal')
@section('content')
<!--Inicio de la seccion de Productos -->
<section id="contenedor-default">
	<h2 class="nos-services">SERVICIOS</h2>

	<div id="cont-serv">

		<div class="serv-left">
			<div class="serv-tp">
				<img src="imgs/serv/events.png" alt="">
				<h2 class="title-serv-one
">EVENTOS</h2>
			</div>

			<div class="serv-tp">
				<img src="imgs/serv/asesoria.png" alt="">
				<h2 class="title-serv-two
">ASESORIAS ESPIRITUALES</h2>
			</div>
			
			<div class="serv-tp">
				<img src="imgs/serv/espiritual.png" alt="">
				<h2 class="title-serv-three
">LIMPIEZAS ESPIRITUALES</h2>
			</div>


			
		</div>
		<div class="serv-right">
		<img src="imgs/serv/budha.jpg" alt="">
			<div class="serv-body">
				
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
				

			</div>
			<div class="cont-button">
					<a class="button-crusta">
					Mas informacion...
					</a>
				</div>

			
		</div>
	
	</div>
</section>
<!-- Fin de la seccion de servicios -->

@stop