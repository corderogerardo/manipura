@extends('layouts.principal')
@section('content')
<div class="white-spacex"></div>
<section id="contenedor-default">
<h2 class="contac-title">CONTACTO</h2>

<div id="cont-contac">

	<div id="form-contac">
		<form action="index_submit" method="POST" accept-charset="utf-8">

		<div class="fil-one">
			<input type="text" name="nombre"  placeholder="Nombre" required>
			<input type="email" name="email"  placeholder="Email" required>
			<input type="tel" name="tel"  placeholder="Telefono" required>
			<input type="text" name="motivo" placeholder="Motivo" required>
		</div>
		<div class="fil-two">
			<textarea name="mensaje" placeholder="Mensaje"></textarea>
		
		</div>
		<div class="cont-button-send">
			<button class="button-send" type="submit">ENVIAR</button>
		</div>
		</form>
		
	</div>

	<div id="contac-img">
	<h3>Manipura</h3>
	<h5>Articulos de Metafisica , Libros , Posters , Información , Todo lo necesario para que el conocimiento llegue a ti...</h5>
	<h5>Cabudare - Lara - Venezuela.</h5>
		<div class="fil-info">
			<i class="fa fa-phone"></i>
			<a href="tel:+582512620402" target="_blank" title="">+58251-2620402</a>
		</div>
		<div class="fil-info">
			<i class="fa fa-mobile"></i>
			<a href="tel:+584122620402" target="_blank" title="">+58412-2620402</a>
		</div>
		<div class="fil-info">
			<i class="fa fa-twitter"></i>
			<a href="https://twitter.com/Manipuralara" target="_blank" title="@manipuralara">@Manipuralara</a>
		</div>
		<div class="fil-info">
			<i class="fa fa-facebook"></i>
			<a href="https://www.facebook.com/profile.php?id=100005412798055" target="_blank" title="Facebook Manipura">Facebook Manipura</a>
		</div>
		<div class="fil-info">
			<i class="fa fa-skype"></i>
			<a href="skype:manipura_lara?call" target="_blank" title="">manipura_lara</a>
		</div>
		<div class="fil-info">
			<i class="fa fa-envelope"></i>
			<a href="mailto:contacto@manipura.com.ve?subject=feedback" target="_blank" "email me">contacto@manipura.com.ve</a>
		</div>
		
	</div>
	
</div>
</section>
@stop