<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Inversiones Manipura</title>
	<!-- <link rel="stylesheet" href="css/style.css"> -->
	<link rel="stylesheet" href="{{elixir('css/style.css')}}">
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">


</head>
<body>

<header id="menuAdmin" class="menuAdmin">

<h1>Manipura - Administrador</h1>
<div id="usuarioAdmin">
	<h2>Alan Indomenico</h2>
</div>
	
</header><!-- /header -->

	@yield('content')

<!-- Inicio del footer -->
<footer>
	<h6>© 2015 Manipura | RIF: J-0000000-0 | Todos los derechos reservados.</h6>
</footer>
<!-- Fin del footer -->
</body>
</html>