<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Inversiones Manipura</title>
	<!-- <link rel="stylesheet" href="css/style.css"> -->
	<link rel="stylesheet" href="{{elixir('css/style.css')}}">

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">


</head>
<body>

<header id="header" class="header">
<!-- Inicio seccion menu -->
<div id="contenedorMenu">
	{{-- Nombre de la web --}}
	<h1>
	
	<img src="imgs/alphabet-material/m.png" alt="">
	<img src="imgs/alphabet-material/a.png" alt="">
	<img src="imgs/alphabet-material/n.png" alt="">
	<img src="imgs/alphabet-material/i.png" alt="">
	<img src="imgs/alphabet-material/p.png" alt="">
	<img src="imgs/alphabet-material/u.png" alt="">
	<img src="imgs/alphabet-material/r.png" alt="">
	<img src="imgs/alphabet-material/a.png" alt="">

	</h1>
	{{-- Menu de la web --}}
	<nav id="menu" class="menu">	
	<ul >
		<li><a class="actived" href="/" title="">Inicio</a></li>
		<li><a href="servicios" title="">Servicios</a></li>
		<li><a href="productos" title="">Productos</a></li>
		<li><a href="contacto" title="">Contacto</a></li>
	</ul>
	</nav>

</div>
<!-- Fin de seccion del Menu -->
</header>
<!-- /header -->

	@yield('content')

<!-- Inicio del footer -->
<footer>
	<h6>© 2015 Manipura | RIF: J-0000000-0 | Todos los derechos reservados.</h6>
</footer>
<!-- Fin del footer -->
</body>
</html>