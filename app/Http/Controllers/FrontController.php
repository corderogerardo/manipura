<?php

namespace Manipura\Http\Controllers;

use Illuminate\Http\Request;
use Manipura\Http\Requests;
use Manipura\Http\Controllers\Controller;

class FrontController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('index');
    }
    public function admin(){
        return view('administrador');
    }
    public function servicios(){
        return view('servicios');
    }
    public function productos(){
        return view('productos');
    }
    public function contacto(){
        return view ('contacto');
    }
}
