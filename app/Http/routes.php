<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/*Rutas de usuario - frontend*/
Route::get('/','FrontController@index');
Route::get('servicios','FrontController@servicios');
Route::get('productos','FrontController@productos');
Route::get('contacto','FrontController@contacto');
Route::get('administrador','FrontController@admin');

/*Rutas de Administrador*/
Route::get('admin','AdminController@index');
Route::get('logout','AdminController@logout');

